-- phpMyAdmin SQL Dump
-- version 4.1.8
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 15 Janvier 2019 à 08:29
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `gestion_de_librairie`
--

-- --------------------------------------------------------

--
-- Structure de la table `achat`
--

CREATE TABLE IF NOT EXISTS `achat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `founiseur` varchar(255) NOT NULL,
  `nombre_examplair` int(11) NOT NULL,
  `prix_unitaire` float NOT NULL,
  `date_achat` date NOT NULL,
  `ISBM` varchar(255) NOT NULL,
  `prix_total` float NOT NULL,
  `montant_verse` float NOT NULL,
  `reste` float NOT NULL,
  `PAR` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Contenu de la table `achat`
--

INSERT INTO `achat` (`id`, `founiseur`, `nombre_examplair`, `prix_unitaire`, `date_achat`, `ISBM`, `prix_total`, `montant_verse`, `reste`, `PAR`) VALUES
(14, 'founiseur', 10, 0, '1901-04-01', 'ISBM', 0, 0, 0, 0),
(15, 'founiseur', 10, 0, '1901-04-01', 'ISBM', 0, 0, 0, 0),
(16, 'founiseur', 10, 0, '1901-04-01', 'ISBM', 0, 0, 0, 0),
(17, 'founiseur', 10, 0, '1901-04-01', 'ISBM', 0, 0, 0, 0),
(18, 'founiseur', 10, 0, '1901-04-01', 'ISBM', 0, 0, 0, 0),
(19, 'founiseur', 10, 0, '1901-04-01', 'ISBM', 0, 0, 0, 0),
(20, 'founiseur', 10, 0, '1901-04-01', 'ISBM', 0, 0, 0, 0),
(21, 'founiseur', 10, 0, '1901-04-01', 'ISBM', 0, 0, 0, 0),
(39, 'TALOM', 30, 1000, '1901-04-01', 'ghufguy', 30000, 0, 0, 0),
(38, 'TALOM', 30, 1000, '1901-04-01', 'ghufguy', 30000, 0, 0, 0),
(37, 'TALOM', 30, 1000, '1901-04-01', 'ghufguy', 30000, 0, 0, 0),
(36, 'TALOM', 30, 1000, '1901-04-01', 'ghufguy', 30000, 0, 0, 0),
(35, 'TALOM', 30, 1000, '1901-04-01', 'ghufguy', 30000, 0, 0, 0),
(34, 'TALOM', 30, 1000, '1901-04-01', 'ghufguy', 30000, 0, 0, 0),
(43, 'JESUS', 10, 100, '1901-04-01', 'ghufguy', 1000, 500, 500, 0),
(44, 'JESUS', 10, 100, '1901-04-01', 'ghufguy', 1000, 500, 500, 0),
(45, 'seul', 10, 100, '2018-12-29', 'ISBM2', 1000, 500, 500, 0),
(46, 'seul', 10, 100, '2018-12-29', 'ISBM2', 1000, 400, 600, 0),
(47, 'seul', 10, 100, '2018-12-29', 'ghufguyrr', 1000, 100, 900, 0),
(51, 'seul', 1000, 1000, '2019-01-08', 'nokiq22', 1000000, 500, 999500, 10),
(49, 'ko', 10, 100, '2019-01-06', 'ghufguy', 1000, 500, 500, 10),
(50, 'seul', 10, 0, '1901-04-01', 'ISBM2', 0, 0, 0, 10);

-- --------------------------------------------------------

--
-- Structure de la table `auteur`
--

CREATE TABLE IF NOT EXISTS `auteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `liste_livre` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `auteur`
--

INSERT INTO `auteur` (`id`, `nom`, `prenom`, `liste_livre`) VALUES
(2, 'TALOM bouopda', 'arnaud', 'hp6033,dell 64000gbugugugu,nokia22,nokia22');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`id`, `nom`, `prenom`, `telephone`, `adresse`) VALUES
(6, 'toto titi', 'tata c', '655667181c', 'bafossam pere');

-- --------------------------------------------------------

--
-- Structure de la table `collection`
--

CREATE TABLE IF NOT EXISTS `collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `id_editeur` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `collection`
--

INSERT INTO `collection` (`id`, `nom`, `id_editeur`) VALUES
(2, 'test', 1000);

-- --------------------------------------------------------

--
-- Structure de la table `editeur`
--

CREATE TABLE IF NOT EXISTS `editeur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `editeur`
--

INSERT INTO `editeur` (`id`, `nom`, `ville`, `telephone`) VALUES
(9, 'asva education', '545', 'douala'),
(8, 'edicef', 'yaounde poste centrale', '+237655667191');

-- --------------------------------------------------------

--
-- Structure de la table `ouvrage`
--

CREATE TABLE IF NOT EXISTS `ouvrage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `nb_page` int(11) NOT NULL,
  `prix_unitaire` float NOT NULL,
  `benefice` float NOT NULL,
  `date_publication` date NOT NULL,
  `ISBM` varchar(255) NOT NULL,
  `qt_stock` int(11) NOT NULL,
  `qt_seuille` int(11) NOT NULL,
  `liste_auteur` varchar(255) NOT NULL,
  `rayon` varchar(255) NOT NULL,
  `prix_vente` float NOT NULL,
  `editeur` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Contenu de la table `ouvrage`
--

INSERT INTO `ouvrage` (`id`, `titre`, `nb_page`, `prix_unitaire`, `benefice`, `date_publication`, `ISBM`, `qt_stock`, `qt_seuille`, `liste_auteur`, `rayon`, `prix_vente`, `editeur`) VALUES
(31, 'Major physique 6', 200, 0, 0, '2019-01-13', '978-9956-423-66-1', 0, 50, ',TALOM bouopda arnaud', 'rayon3', 0, 'asva education');

-- --------------------------------------------------------

--
-- Structure de la table `rayon`
--

CREATE TABLE IF NOT EXISTS `rayon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `rayon`
--

INSERT INTO `rayon` (`id`, `nom`) VALUES
(5, 'rayon3'),
(4, 'roman '),
(6, 'RAYON4');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `indice` varchar(255) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `password`, `indice`, `telephone`, `adresse`, `user_name`, `type`) VALUES
(21, 'TALLA', 'KEVIN', '12345', 'EASYLIBRAIRIE', '+237655667190', 'YAOUNDE MONTI', 'TAL_K', 'Libraire'),
(15, 'TALOM', 'ARNAUD', '12345', 'EASYLIBRAIRIE', '+237655667191', 'YAOUNDE BONAS', 'PICOLO', 'Administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `vente`
--

CREATE TABLE IF NOT EXISTS `vente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` varchar(255) NOT NULL,
  `articles` text NOT NULL,
  `date` date NOT NULL,
  `prix_total_a_payer` float NOT NULL,
  `benefice` float NOT NULL,
  `id_vendeur` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=130 ;

--
-- Contenu de la table `vente`
--

INSERT INTO `vente` (`id`, `id_client`, `articles`, `date`, `prix_total_a_payer`, `benefice`, `id_vendeur`) VALUES
(115, 'Inconnu', 'nokiq22!3!3300.0!#', '2019-01-09', 3300, 300, 1),
(116, 'Inconnu', 'nokiq22!3!3300.0!#nokiq22!84!92400.0!#', '2019-01-09', 95700, 8700, 1),
(117, 'Inconnu', 'nokiq22!897!986700.0!#', '2019-01-09', 986700, 89700, 1),
(118, 'Inconnu', 'nokiq22!12!13200.0!#', '2019-01-09', 13200, 1200, 1),
(119, 'Inconnu', 'nokiq22!12!13200.0!#', '2019-01-09', 13200, 1200, 1),
(120, 'Inconnu', 'nokiq22!2!2200.0!#', '2019-01-09', 2200, 1200, 1),
(121, 'Inconnu', 'nokiq22!4!4400.0!#', '2019-01-09', 4400, 1200, 1),
(122, 'Inconnu', 'nokiq22!4!4400.0!#', '2019-01-09', 4400, 400, 1),
(123, 'Inconnu', 'nokiq22!12!13200.0!#', '2019-01-09', 13200, 1200, 1),
(126, 'Inconnu', 'nokiq22!10!11000.0!#nokiq22!11!12100.0!#', '2019-01-09', 23100, 2100, 1),
(127, 'Inconnu', 'nokiq22!4!4400.0!#nokiq22!4!4400.0!#', '2019-01-12', 8800, 800, 1),
(128, 'Inconnu', 'nokiq22!12!13200.0!#nokiq22!12!13200.0!#nokiq22!12!13200.0!#', '2019-01-12', 39600, 3600, 1),
(129, 'Inconnu', 'nokiq22!13!14300.0!#', '2019-01-12', 14300, 1300, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
